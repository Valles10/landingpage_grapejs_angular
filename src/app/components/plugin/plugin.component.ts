import { Component, OnInit } from '@angular/core';
// @ts-ignore
import * as grapesjs from 'grapesjs';
import 'node_modules/grapesjs-custom-code';
import 'node_modules/grapesjs-component-code-editor';
import 'node_modules/grapesjs-preset-webpage';


@Component({
  selector: 'app-plugin',
  templateUrl: './plugin.component.html',
  styleUrls: ['./plugin.component.css']
})
export class PluginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const editor = grapesjs.init({
      container : '#gjs',
      fromElement: true,
      storageManager: {
        type : 'local',
      },
      plugins: ['gjs-preset-webpage','grapesjs-custom-code','grapesjs-component-code-editor'],
      pluginsOpts: {
        'gjs-preset-newsletter': {
          modalTitleImport: 'Import template',
          modalLabelImport: 'default',
          cellStyle: {
            padding: '10px',
          },
          // ... other option
        }
      }
  });

  const pn = editor.Panels;
const panelViews = pn.addPanel({
  id: 'views'
});
panelViews.get('buttons').add([{
  attributes: {
     title: 'Open Code'
  },
  className: 'fa fa-file-code-o',
  command: 'open-code',
  togglable: false, //do not close when button is clicked again
  id: 'open-code'
}]);
  }

}
